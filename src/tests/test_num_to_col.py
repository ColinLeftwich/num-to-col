import string

from src.lib import num_to_col

def test_num_to_col():
    for top_num, top_char in enumerate(string.ascii_uppercase):
        answer = top_char
        number = top_num
        assert answer == num_to_col(top_num)

        for middle_num, middle_char in enumerate(string.ascii_uppercase):
            answer = top_char + middle_char
            number = (top_num * 26) + middle_num + 26
            assert answer == num_to_col(number)

            for bottom_num, bottom_char in enumerate(string.ascii_uppercase):
                answer = top_char + middle_char + bottom_char
                number = (top_num * 676) + (middle_num * 26) + bottom_num + 702
                assert answer == num_to_col(number)