from icecream import ic

TOTAL_CHARACTERS: int = 26

def count_sets(number: int) -> tuple[int, int]:
    count: int = 0

    while number > TOTAL_CHARACTERS:
        count += 1
        number -= TOTAL_CHARACTERS
    
    return (count, number)

def num_to_col(number: int)-> str:
    UNICODE_OFFSET: int = 64

    result: str = ""
    numbers: list[int] = []

    count, remainder, = count_sets(number + 1)
    numbers.append(remainder)

    while count > TOTAL_CHARACTERS:
        count, remainder = count_sets(count)
        numbers.append(remainder)

    if count > 0:
        numbers.append(count)

    numbers.reverse()

    for num in numbers:
        result = result + chr(num + UNICODE_OFFSET)

    return result

if __name__ == "__main__":
    number = int(input()) - 1

    ic(num_to_col(number))