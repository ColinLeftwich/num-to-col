set shell := ["powershell.exe", "-c"]
run: check 
    python src/lib.py

check:
    ruff check src/
    mypy src/lib.py

test: 
    pytest -s
